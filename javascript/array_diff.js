// http://www.codewars.com/kata/523f5d21c841566fde000009/train/javascript
//
function array_diff(a, b) {
  var b_has = b.reduce(function (acc, curr) {
    if (typeof acc[curr] == 'undefined') {
      acc[curr] = 1;
    }
    return acc;
  }, {});

  return a.filter(function(elm) {
    return (typeof b_has[elm] == 'undefined');
  });
}
