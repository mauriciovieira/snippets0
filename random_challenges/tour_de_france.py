def build_hash(city_map):
    paths = {} 
    for path in city_map:
        origin, dest = path
        paths[origin] = dest
    # paths = {"Evry": "Paris","Grenoble": "Risoul", "Risoul": "Evry" }
    return paths


class LinkedList:
    next = None
    element = ""
    
    def __init__(self, element, next):
        self.element = element
        self.next = next

    def __str__(self):
        result = ['[', self.element]
        next_elem = self.next
        while next_elem:
            result.append(', ')
            result.append(next_elem.element)
            next_elem = next_elem.next
        result.append(']')
        return '"'.join(result)


def reverse_dict(dict = {}):
    # paths = {"Paris": "Evry", "Risoul": "Grenoble", "Evry": "Risoul" }

    reversed_dict = {}
    for key, value in dict.items():
        reversed_dict[value] = key
    return reversed_dict


def build_list(city_map, paths = {}):
    
    first_city = city_map[0][0]
    last_city = city_map[0][1]
    
    tail = LinkedList(last_city, None)
    root = LinkedList(first_city, tail)
    
    # Start from any point and go backwards
    reversed_paths = reverse_dict(paths)
    while reversed_paths.get(first_city):
        previous_city = reversed_paths.get(first_city)
        new_root = LinkedList(previous_city, root)
        root = new_root
        first_city = previous_city
    
    # Then go back to that point and go forward.
    while paths.get(last_city):    
        next_city = paths.get(last_city)
        new_tail = LinkedList(next_city, None)
            
        tail.next = new_tail
        last_city = next_city
        tail = new_tail
        
    return root
 

def main():
    city_map = [ ("Evry", "Paris"), ("Grenoble", "Risoul"), ("Risoul", "Evry") ]
    output = '["Grenoble", "Risoul", "Evry", "Paris"]'

    paths = build_hash(city_map)
    root = build_list(city_map, paths)
    result = str(root)
    print(result)


if __name__ == "__main__":
    main()
