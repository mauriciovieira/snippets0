xml_valid1 = '''<place name="Netherlands">
<place name="Amsterdam">
</place>
</place>'''

xml_valid2 = '''<continent name="Europe">
<country name="Netherlands">
</country>
</continent>
'''

xml_invalid = '''<continent name="Europe">
<country name="Netherlands">
</continent>
</country>'''


def get_tag_name(line):
    letters = []

    not_allowed = ['<', '/', '>']
    for letter in line:
        if letter == ' ':
            break
        elif letter in not_allowed:
            pass
        else:
            letters.append(letter)

    return ''.join(letters)


def tag_is_opening(line):
    for letter in line:
        if letter == '/':
            return False
    return True


def is_valid(xml_strings_list):
    """Returns if the string contains valid xml"""

    stack = []

    for line in xml_strings_list:
        tag_name = get_tag_name(line)
        is_opening = tag_is_opening(line)

        if is_opening:
            stack.append(tag_name)
        else:
            if not stack:
                return False

            top = stack.pop()
            if top != tag_name:
                return False

    return True


def tests():
    def test_get_tag_name():
        expected = 'continent'
        result = get_tag_name('<continent name="Europe">')

        assert expected == result, result
        
        result = get_tag_name('</continent>')

        assert expected == result, result

    def test_tag_is_opening():
        expected = True
        result = tag_is_opening('<continent name="Europe">')

        assert expected == result, result
        
        expected = False
        result = tag_is_opening('</continent>')

        assert expected == result, result
    
    def test_is_valid():
        valid_xml = ['<continent>', '<country>', '</country>', '</continent>']
        expected = True
        result = is_valid(valid_xml)
        assert expected == result, result

        invalid_xml = ['<continent>', '<country>', '</continent>', '</country>']
        expected = False
        result = is_valid(invalid_xml)
        assert expected == result, result

        invalid_xml = ['</continent>', '</country>']
        expected = False
        result = is_valid(invalid_xml)
        assert expected == result, result

        invalid_xml = ['<continent>', '</continent>', '</country>']
        expected = False
        result = is_valid(invalid_xml)
        assert expected == result, result

        invalid_xml = ['<country>', '</continent>']
        expected = False
        result = is_valid(invalid_xml)
        assert expected == result, result

        valid_xml = xml_valid1.split('\n')
        expected = True
        result = is_valid(valid_xml)
        assert expected == result, result

        valid_xml = xml_valid2.split('\n')
        expected = True
        result = is_valid(valid_xml)
        assert expected == result, result

        invalid_xml = xml_invalid.split('\n')
        expected = False
        result = is_valid(invalid_xml)
        assert expected == result, result

    test_get_tag_name()
    test_tag_is_opening()
    test_is_valid()


if __name__ == '__main__':
    tests()