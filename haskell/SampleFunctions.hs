module SampleFunctions where

abs :: Int -> Int
abs n = if n >= 0 then n else -n

--nabs :: Num -> Num
--nabs n = if n >= 0 then n else -n
signum :: Int -> Int
signum n = if n > 0 then 1 else
              if n < 0 then -1 else 0

gabs :: Int -> Int
gabs n | n >= 0 = n
       | otherwise = -n


