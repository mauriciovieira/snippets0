module Halves where

{-}
halveA :: [a] -> ([a], [a])
halveA xs = (take n xs, drop n xs)
  where n = length xs / 2
-}

halveAfixed :: [a] -> ([a], [a])
halveAfixed xs = (take n xs, drop n xs)
  where n = length xs `div` 2

halveB :: [a] -> ([a], [a])
halveB xs = splitAt (length xs `div` 2) xs

halveC :: [a] -> ([a], [a])
halveC xs = (take (n `div` 2) xs, drop (n `div` 2) xs)
  where n = length xs

{-
halveD :: [a] -> ([a], [a])
halveD xs = splitAt (length xs `div` 2)
-}

